# Generated by Django 2.2.2 on 2019-07-31 23:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='banner',
        ),
        migrations.RemoveField(
            model_name='product',
            name='cod',
        ),
        migrations.RemoveField(
            model_name='product',
            name='quantidade',
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
