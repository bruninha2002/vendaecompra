from django.shortcuts import render
from django.views.generic import ListView , DetailView
from django.shortcuts import redirect

# Create your views here.
from blog.models import Product 

class ProductListView(ListView):
    model = Product

class OutofStockListView(ListView):
    model = Product 

    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(quantidade=0)

class AddStockView(DetailView):
    model = Product

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade += int(quantidade)
        product.save()
        return redirect('product_list')

class RemoveStockView(DetailView):
    model = Product

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade -= int(quantidade)
        product.save()
        return redirect('product_list')